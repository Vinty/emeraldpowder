﻿using UnityEngine;

namespace Env {
    public enum ResourceKind {
        FOOD,
        TREE,
        STONE,
        GOLD
    }

    [RequireComponent(typeof(Rigidbody))]
    public class Resource : MonoBehaviour {
        [SerializeField] private ResourceKind resourceKind;

        private void Awake() {
            ResKind = resourceKind;
        }

        public ResourceKind ResKind  {get; set;}
        public int          ResourceCount {get; set;}

        public ResourceKind GetResourceKind() {
            return resourceKind;
        }

        // public Resource(ResourceKind resKind, int resourceCount) {
        //     this.ResKind  = resKind;
        //     this.ResourceCount = resourceCount;
        // }
    }
}
