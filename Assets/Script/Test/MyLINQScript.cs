﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class MyLinqScript : MonoBehaviour {
	[SerializeField] private List<GameObject> gameObjectList;

	private string[]                      myNames = { "aaa", "bbb", "ccc", "DD", "WW", "GGGGGG" };
	private Dictionary<int, List<string>> dic     = new Dictionary<int, List<string>>();
	private Vector2Int                    range   = new Vector2Int(0, 3);

	private void Start() {
		myNames.ToList().ForEach(AddToDic);
		dic.ToList().ForEach(y => {
			                     print($"{y.Key}");
			                     y.Value.ForEach(print);});
	}

	private GameObject GetNearestGameObject() {
		return gameObjectList
		      .OrderBy(x => Vector3.Distance(transform.position, x.transform.position))
		      .FirstOrDefault();
	}

	private GameObject[] GetNearestMinHealth() {
		return gameObjectList
		      .OrderBy(x => Vector3.Distance(transform.position, x.transform.position))
		      .ThenBy(x => x.tag)
		      .Take(4)
		      .ToArray();
	}

	private IEnumerator GetNearestGameObject(List<GameObject> go) {
		var coinsByDistance = go.OrderBy(x => Vector3.Distance(transform.position, x.transform.position));
		yield return new WaitForSeconds(1f);

		if(Input.GetKeyDown(KeyCode.A)) {
			foreach(var coin in coinsByDistance) {
				// coin.DoSomething();
			}
		}
		else {
			Debug.Log("-------------");
		}
	}

	private void AddToDic(string name) {
		int counter = Random.Range(range.x, range.y);

		if(!dic.ContainsKey(counter)) {
			dic.Add(counter, new List<string> { name });
			return;
		}

		dic[counter].Add(name);
	}
}
