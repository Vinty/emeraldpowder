﻿using System.Collections.Generic;
using System.IO;
using UnityEngine;

// using Google.Apis.Auth.OAuth2;
// using Google.Apis.Sheets.v4;
// using Google.Apis.Sheets.v4.Data;
// using Google.Apis.Services;
// using Google.Apis.Util.Store;

public class PersonGenerator : MonoBehaviour {
	private const string FAMILY = "Assets/Family"; 
	private const string NAMES_MAN = "Assets/Family"; 
	private const string NAMES_WOMAN = "Assets/Family"; 
	
	private Dictionary<string, Person> inputPersons = new Dictionary<string, Person>();

	private List<List<List<string>>> rawData = new List<List<List<string>>>();

	private string[] namesManEng = {};
	private string[] namesManRus = {};
	private string[] namesWomanEng = {};
	private string[] namesWomanRus = {};
	private string[] familyEng;
	private string[] familyRus;

	private void Start() {
		// ParseFile(FAMILY);
		// ParseFile(NAMES_MAN);
		// ParseFile(NAMES_WOMAN);
	}

	private void ParseFile(string path) {
		var      fileData = File.ReadAllText(path);
		string[] lines    = fileData.Split("\n"[0]);
		for(int i = 0; i < lines.Length; i++) {
			var lineData = (lines[i].Trim()).Split(","[0]);
			
			print($"{lineData[0]} {lineData[1]}");
		}
	}
}
