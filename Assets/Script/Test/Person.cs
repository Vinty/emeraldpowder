﻿public class Person {
	public string lang   {get; set;}
	public string name   {get; set;}
	public string family {get; set;}

	public Person(string lang, string name, string family) {
		this.lang   = lang;
		this.name   = name;
		this.family = family;
	}
}
