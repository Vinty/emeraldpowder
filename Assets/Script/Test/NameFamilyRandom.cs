﻿using System.Collections.Generic;
using UnityEngine;

public class NameFamilyRandom : MonoBehaviour {
    private string[] myNames = { "Alex", "Mike", "Vital", "Mark", "Archibaldo" };
    private string[] family  = { "Ushakov", "Ziskunov", "Belosheev", "Artukevich", "Beladjo" };

    private void Start() {
        for(int i = 0; i < 10; i++) {
            print(GeneratorFullNames());
        }
    }

    private string RandomElementFromArray(string[] array) {
        return array[Random.Range(0, array.Length)];
    }

    private string GeneratorFullNames() {
        return RandomElementFromArray(myNames) + " " + RandomElementFromArray(family);
    }
}
