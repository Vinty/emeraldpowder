﻿using Env;
using UnityEngine;

namespace Animation {
    public class FarmAnimations : MonoBehaviour {
        private static readonly int treeAnimationBool         = Animator.StringToHash("TreeAnimationBool");
        private static readonly int treeAnimationOnGroundBool = Animator.StringToHash("TreeAnimationOnGroundBool");
        private static readonly int goldAnimationBool         = Animator.StringToHash("GoldAnimationBool");

        public void StartAnimatorResource(GameObject go) {
            Animator     animator     = go.GetComponentInChildren<Animator>();
            ResourceKind resourceKind = go.GetComponent<Resource>().ResKind;
            animator.SetBool(GetAnimatorClip(resourceKind), true);
        }

        private int GetAnimatorClip(ResourceKind resourceKind) {
            switch(resourceKind) {
                case ResourceKind.TREE :
                case ResourceKind.FOOD :
                    return treeAnimationBool;
                case ResourceKind.GOLD :
                case ResourceKind.STONE :
                    return goldAnimationBool;
                default : return 0;
            }
        }

        public void StopAnimatorTree(GameObject go) {
            Animator animator = go.GetComponentInChildren<Animator>();
            animator.SetBool(treeAnimationBool, false);
            // Rebind() - новый аналог Stop() (deprecated)
            animator.Rebind();
        }

        public void StartAnimatorTreeOnGround(GameObject go) {
            Animator animator = go.GetComponentInChildren<Animator>();
            animator.SetBool(treeAnimationOnGroundBool, true);
        }

        public void StopAnimatorTreeOnGround(GameObject go) {
            Animator animator = go.GetComponentInChildren<Animator>();
            animator.SetBool(treeAnimationOnGroundBool, false);
        }
    }
}
