﻿using UnityEngine;

public class TooltipText : MonoBehaviour {
	[SerializeField] private string handWrittenText;

	public string HandWrittenText {get; set;}
	public string Text            {get; set;}

	private void Awake() {
		HandWrittenText = $"{handWrittenText} \n";
	}
}
