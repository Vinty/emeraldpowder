﻿using System.Collections;
using BuildingScripts;
using Env;
using UnityEngine;
using UnityEngine.AI;

namespace Chars {
    public enum StatePorter {
        FIND_RESOURCE,
        MOVE_TO_MINE,
        MINING,
        PICK_UP_RESOURCES,
        RETURN_TO_HOME,
        UNLOADING,
        WAIT
    }

    public class Porter : MonoBehaviour {
        public StatePorter CurrentState    {get; set;}
        public GameObject  CurrentResource {get; set;}

        [SerializeField] private GameObject home;
        [SerializeField] private int        currentWorkerLevel;
        [SerializeField] private GameObject extractedResourcePrefab;
        [SerializeField] private Transform  bagPoint;

        public  Mine         CurrentMine {get; set;}
        
        private FarmManager  farmManager;
        private NavMeshAgent agent;
        private WorkerLevel  workerLevel;
        private Mine         mine;
        private Resource     extractedResource;
        private int          speed;
        private Vector2Int   resourceCount;
        private MainMenu     mainMenu;

        private Coroutine coroutine;

        private void Awake() {
            farmManager   = FindObjectOfType<FarmManager>();
            mainMenu      = FindObjectOfType<MainMenu>();
            agent         = GetComponent<NavMeshAgent>();
            workerLevel   = WorkerLevel.getInstance()[currentWorkerLevel];
            speed         = workerLevel.Speed;
            resourceCount = workerLevel.ResourceCount;
        }

        private void Update() {
            switch(CurrentState) {
                case StatePorter.FIND_RESOURCE :
                    CurrentMine = FindMineWithResources();
                    if(CurrentMine != null) {
                        mine         = CurrentMine;
                        CurrentState = StatePorter.MOVE_TO_MINE;
                    }

                    break;
                case StatePorter.MOVE_TO_MINE :
                    if(coroutine == null) {
                        coroutine = StartCoroutine(MoveToTarget(CurrentMine.transform.position));
                    }

                    break;
                case StatePorter.MINING :
                    if(mine.IsExtractedResourcesQueueNotEmpty()) {
                        extractedResource = mine.GetExtractedResourceFromQueue();
                        CurrentState      = StatePorter.PICK_UP_RESOURCES;
                        break;
                    }

                    CurrentState = StatePorter.RETURN_TO_HOME;

                    break;
                case StatePorter.PICK_UP_RESOURCES :
                    CurrentResource = CreateResourcePack();
                    CurrentResource.transform.SetParent(gameObject.transform);
                    CurrentState = StatePorter.RETURN_TO_HOME;
                    break;
                case StatePorter.RETURN_TO_HOME :
                    if(coroutine == null) {
                        coroutine = StartCoroutine(MoveToTarget(home.transform.position));
                    }

                    break;
                case StatePorter.UNLOADING :
                    Destroy(CurrentResource.gameObject);
                    mainMenu.AddResource(extractedResource);
                    CurrentState = StatePorter.FIND_RESOURCE;
                    break;
                case StatePorter.WAIT : break;
            }
        }

        private Mine FindMineWithResources() {
            return farmManager.GetMineWithResourcesFromQueue();
        }

        private IEnumerator MoveToTarget(Vector3 targetPos) {
            agent.speed = speed;
            agent.SetDestination(targetPos);
            yield break;
        }

        private GameObject CreateResourcePack() {
            return Instantiate(extractedResourcePrefab, bagPoint.position, Quaternion.identity);
        }
    }
}
