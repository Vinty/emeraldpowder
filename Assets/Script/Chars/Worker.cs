﻿using System.Collections;
using Animation;
using BuildingScripts;
using Env;
using UnityEngine;
using UnityEngine.AI;

namespace Chars {
	public enum StateWorker {
		FIND_RESOURCE,
		MOVE_TO_MINING,
		MINING,
		PICK_UP_RESOURCES,
		CRAFT,
		PREPARE_RETURN_TO_HOME,
		MOVE_TO_HOME,
		UNLOADING,
		WAIT
	}

	public class Worker : MonoBehaviour {
		[SerializeField] private int currentWorkerLevel;

		// [SerializeField] private GameObject   home;
		[SerializeField] private GameObject extractedResourcePrefab;
		[SerializeField] private Transform  bagPoint;

		private const string TOOLTIP_CONST_TEXT = "Worker \n ";

		public GameObject   Home            {get; set;}
		public StateWorker  CurrentState    {get; set;}
		public GameObject   CurrentResource {get; set;}
		public ResourceKind ResourceKind    {get; set;}

		private WorkerLevel    workerLevel;
		private FarmAnimations farmAnimations;
		private TooltipText    tooltipText;

		public Mine Mine {get; set;}

		private FarmManager  farmManager;
		private NavMeshAgent agent;
		private int          speed;
		private int          miningRate;

		private Vector2Int resourceCount;

		private Vector3   moveSpeed;
		private Resource  extractedResource;
		private Coroutine coroutine;

		private void Start() {
			farmAnimations = FindObjectOfType<FarmAnimations>();
			workerLevel    = WorkerLevel.getInstance()[currentWorkerLevel];
			speed          = workerLevel.Speed;
			miningRate     = workerLevel.MiningRate;
			resourceCount  = workerLevel.ResourceCount;

			agent        = GetComponent<NavMeshAgent>();
			farmManager  = FindObjectOfType<FarmManager>();
			CurrentState = StateWorker.FIND_RESOURCE;
			agent.speed  = speed;
			Mine         = Home.GetComponent<Mine>();
			tooltipText  = GetComponent<TooltipText>();
		}

		private void Update() {
			switch(CurrentState) {
				case StateWorker.FIND_RESOURCE :
					CurrentResource = FindTarget();

					if(CurrentResource != null) {
						transform.LookAt(CurrentResource.transform);
						CurrentState = StateWorker.MOVE_TO_MINING;
					}

					break;
				case StateWorker.MOVE_TO_MINING :
					coroutine = StartCoroutine(MoveToTarget(CurrentResource.transform.position));
					break;
				case StateWorker.MINING :
					agent.speed = 0;
					farmAnimations.StartAnimatorResource(CurrentResource);
					StartCoroutine(PauseForMining(miningRate));
					CurrentState = StateWorker.WAIT;
					break;
				case StateWorker.PICK_UP_RESOURCES :
					extractedResource               = CurrentResource.GetComponent<Resource>();
					extractedResource.ResKind       = GetResourceKind();
					extractedResource.ResourceCount = AddResourceRandomCount();

					CurrentState = StateWorker.CRAFT;
					break;
				case StateWorker.CRAFT :
					if(coroutine == null) {
						coroutine = StartCoroutine(PauseForCraft(CurrentResource, 2));
					}

					break;
				case StateWorker.PREPARE_RETURN_TO_HOME :
					if(coroutine != null) {
						StopCoroutine(coroutine);
						coroutine = null;
					}

					Destroy(CurrentResource.gameObject);
					farmManager.RestoreRemovedResources(ResourceKind);
					CurrentResource = CreateLog();
					CurrentResource.transform.SetParent(gameObject.transform);
					CurrentState = StateWorker.MOVE_TO_HOME;
					break;
				case StateWorker.MOVE_TO_HOME :
					if(coroutine == null) {
						coroutine = StartCoroutine(MoveToTarget(Home.transform.position));
					}

					break;
				case StateWorker.UNLOADING :
					if(coroutine != null) {
						StopCoroutine(coroutine);
						coroutine = null;
					}

					// ReSharper disable once Unity.IncorrectMonoBehaviourInstantiation
					Mine.AddResourceToQueue(extractedResource);
					Destroy(CurrentResource.gameObject);
					CurrentState = StateWorker.FIND_RESOURCE;
					break;
				case StateWorker.WAIT : break;
			}
		}

		public void SetTooltipText(string text) {
			GetComponent<TooltipText>().Text = $"{TOOLTIP_CONST_TEXT} {text}";
		}

		private GameObject FindTarget() {
			return farmManager.RemoveInstantiateResToMap(ResourceKind);
		}

		private GameObject CreateLog() {
			return Instantiate(extractedResourcePrefab, bagPoint.position, Quaternion.identity);
		}

		private ResourceKind GetResourceKind() {
			return CurrentResource.gameObject.GetComponentInParent<Resource>().GetResourceKind();
		}

		private IEnumerator PauseForCraft(GameObject go, int timeInSecond) {
			farmAnimations.StartAnimatorTreeOnGround(go);
			yield return new WaitForSeconds(timeInSecond);
			farmAnimations.StopAnimatorTreeOnGround(go);
			yield return new WaitForSeconds(1.5f);
			CurrentState = StateWorker.PREPARE_RETURN_TO_HOME;
		}

		private int AddResourceRandomCount() {
			return Random.Range(resourceCount.x, resourceCount.y + 1);
		}

		private IEnumerator PauseForMining(int timeInSecondMiningRate) {
			yield return new WaitForSeconds(timeInSecondMiningRate);
			farmAnimations.StopAnimatorTree(CurrentResource);
			CurrentState = StateWorker.PICK_UP_RESOURCES;
		}

		private IEnumerator MoveToTarget(Vector3 targetPos) {
			agent.speed = speed;
			agent.SetDestination(targetPos);
			yield break;
		}
	}
}
