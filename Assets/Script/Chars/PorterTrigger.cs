﻿using BuildingScripts;
using UnityEngine;

namespace Chars {
    public class PorterTrigger : MonoBehaviour {
        private Porter porter;

        private void Start() {
            porter = GetComponent<Porter>();
        }

        private void OnTriggerEnter(Collider other) {
            if(other == null) {
                return;
            }

            string currentTag = other.tag;

            switch(currentTag) {
                case"Warehouse" :
                    if(porter.CurrentState == StatePorter.RETURN_TO_HOME) {
                        porter.CurrentState = StatePorter.UNLOADING;
                    }

                    break;
                case"ResourceHome" :
                    if(porter.CurrentState             == StatePorter.MOVE_TO_MINE
                    && porter.CurrentMine.ResourceKind == other.GetComponent<Mine>().ResourceKind) {
                        porter.CurrentState = StatePorter.MINING;
                    }

                    break;
            }
        }
    }
}
