﻿using BuildingScripts;
using Env;
using UnityEngine;

namespace Chars {
    public class WorkerTrigger : MonoBehaviour {
        private Worker worker;

        private void Awake() {
            worker = GetComponent<Worker>();
        }

        private void OnTriggerEnter(Collider other) {
            if(other == null) {
                return;
            }

            string currentTag = other.tag;

            switch(currentTag) {
                case"Resource" :
                    if(worker.CurrentState == StateWorker.MOVE_TO_MINING
                    && worker.ResourceKind == other.GetComponent<Resource>().ResKind) {
                        StartMining(other.gameObject);
                    }

                    break;
                case"ResourceHome" :
                    if(worker.CurrentState == StateWorker.MOVE_TO_HOME
                    && worker.ResourceKind == other.GetComponent<Mine>().ResourceKind) {
                        worker.CurrentState = StateWorker.UNLOADING;
                    }

                    break;
            }
        }

        private void StartMining(GameObject go) {
            worker.CurrentResource = go;
            worker.CurrentState    = StateWorker.MINING;
        }
    }
}
