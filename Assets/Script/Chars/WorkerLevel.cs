﻿using System.Collections.Generic;
using UnityEngine;

namespace Chars {
    public enum IntelligenceLevel {
        STUPID,
        NORMAL,
        GENIUS
    }

    public class WorkerLevel {
        public int               Level             {get;}
        public int               Speed             {get;}
        public int               MiningRate        {get;}
        public int               CraftRate         {get;}
        public IntelligenceLevel IntelligenceLevel {get;}
        public Vector2Int        ResourceCount     {get;}

        private static Dictionary<int, WorkerLevel> instance = null;

        public static Dictionary<int, WorkerLevel> getInstance() {
            return instance ?? (getWorkerLevel());
        }

        private WorkerLevel(int level, int speed, int miningRate, int craftRate, IntelligenceLevel intelligenceLevel,
                            Vector2Int resourceCount) {
            this.Level             = level;
            this.Speed             = speed;
            this.MiningRate        = miningRate;
            this.CraftRate         = craftRate;
            this.IntelligenceLevel = intelligenceLevel;
            this.ResourceCount     = resourceCount;
        }

        private static Dictionary<int, WorkerLevel> getWorkerLevel() {
            Dictionary<int, WorkerLevel> workerLevels = new Dictionary<int, WorkerLevel> {
                { 1, new WorkerLevel(1, 1, 15, 20, IntelligenceLevel.STUPID, new Vector2Int(1, 2)) },
                { 2, new WorkerLevel(2, 2, 13, 16, IntelligenceLevel.STUPID, new Vector2Int(1, 3)) },
                { 3, new WorkerLevel(3, 3, 11, 12, IntelligenceLevel.NORMAL, new Vector2Int(2, 3)) },
                { 4, new WorkerLevel(4, 4, 9,  9,  IntelligenceLevel.NORMAL, new Vector2Int(2, 4)) },
                { 5, new WorkerLevel(5, 5, 7,  3,  IntelligenceLevel.GENIUS, new Vector2Int(4, 5)) }
            };
            return workerLevels;
        }
    }
}
