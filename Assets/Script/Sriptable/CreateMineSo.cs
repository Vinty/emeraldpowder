﻿using Env;
using UnityEngine;

namespace Sriptable {
    [CreateAssetMenu(fileName = "CreateMine", menuName = "CreateObjects/LumberMine", order = 0)]
    public class CreateMineSo : ScriptableObject {
        public bool         isActive;
        public int          levelMine;
        public ResourceKind resourceKind;
    }
}
