﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using BuildingScripts;
using Chars;
using Env;
using UnityEngine;

public class FarmManager : MonoBehaviour {
	[SerializeField] private List<Resource>   allResourcesList;
	[SerializeField] private List<GameObject> allResourcePoints;
	[SerializeField] private List<GameObject> mines;
	[SerializeField] private Worker           worker;
	[SerializeField] private int              countTree;
	[SerializeField] private int              countFood;
	[SerializeField] private int              countStone;
	[SerializeField] private int              countGold;
	[SerializeField] private float            pauseBeforeRestoreResource = 2f;

	private          Dictionary<ResourceKind, List<GameObject>>  resourcesMapForInstant;
	private          Dictionary<ResourceKind, Queue<GameObject>> resourceMapInstantiatedResources;
	private readonly Queue<Mine>                                 minesQueue = new Queue<Mine>();
	private          Platform                                    platform;

	private void Start() {
		platform                         = FindObjectOfType<Platform>();
		resourcesMapForInstant           = new Dictionary<ResourceKind, List<GameObject>>();
		resourceMapInstantiatedResources = new Dictionary<ResourceKind, Queue<GameObject>>();

		InitialLoadingResourcesInMap(allResourcesList, ResourceKind.TREE,  countTree);
		InitialLoadingResourcesInMap(allResourcesList, ResourceKind.FOOD,  countFood);
		InitialLoadingResourcesInMap(allResourcesList, ResourceKind.STONE, countStone);
		InitialLoadingResourcesInMap(allResourcesList, ResourceKind.GOLD,  countGold);

		CreateMine(ResourceKind.TREE);
		CreateMine(ResourceKind.FOOD);
		CreateMine(ResourceKind.GOLD);
		CreateMine(ResourceKind.STONE);
	}

	public void RestoreRemovedResources(ResourceKind resourceKind) {
		if(resourceMapInstantiatedResources[resourceKind].Count < countTree) {
			StartCoroutine(
				InitialLoadingResourcesInMapWithPause(allResourcesList, resourceKind, 1, pauseBeforeRestoreResource)
			);
		}
	}

	public void RegistrationMine(Mine mineGameObject) {
		minesQueue.Enqueue(mineGameObject);
	}

	public Mine GetMineWithResourcesFromQueue() {
		return minesQueue.Count < 1 ? null : minesQueue.Dequeue();
	}

	private IEnumerator InitialLoadingResourcesInMapWithPause(List<Resource> resources, ResourceKind resourceKind,
	                                                          int            i,         float timeRestoreResource) {
		yield return new WaitForSeconds(timeRestoreResource);
		InitialLoadingResourcesInMap(resources, resourceKind, i);
	}

	private void InitialLoadingResourcesInMap(List<Resource> fromList, ResourceKind resourceKind, int resourceCount) {
		fromList.ForEach(AddAllResToMap);

		for(int i = 0 ;
			i < resourceCount ;
			i++) {
			GameObject instantiate = Instantiate(GetRandomResourceFromMap(resourceKind));
			AddInstantiateResToMap(instantiate);

			GameObject randomPoint = GetRandomGOFromFreeResourcePoints();
			instantiate.transform.position = randomPoint.transform.position;
			instantiate.transform.SetParent(randomPoint.transform);
		}
	}

	private void AddInstantiateResToMap(GameObject resourceGO) {
		var resKind = resourceGO.GetComponent<Resource>().ResKind;
		if(!resourceMapInstantiatedResources.ContainsKey(resKind)) {
			resourceMapInstantiatedResources.Add(resKind, new Queue<GameObject>());
			resourceMapInstantiatedResources[resKind].Enqueue(resourceGO);
			return;
		}

		resourceMapInstantiatedResources[resKind].Enqueue(resourceGO);
	}

	public GameObject RemoveInstantiateResToMap(ResourceKind resKind) {
		if(resourceMapInstantiatedResources[resKind].Count == 0) {
			return null;
		}

		return resourceMapInstantiatedResources.ContainsKey(resKind)
			? resourceMapInstantiatedResources[resKind].Dequeue()
			: null;
	}

	private void AddAllResToMap(Resource res) {
		if(!resourcesMapForInstant.ContainsKey(res.ResKind)) {
			resourcesMapForInstant.Add(res.ResKind, new List<GameObject>() { res.gameObject });
			return;
		}

		resourcesMapForInstant[res.ResKind].Add(res.gameObject);
	}

	private GameObject GetRandomResourceFromMap(ResourceKind resourceKind) {
		List<GameObject> gameObjects = resourcesMapForInstant[resourceKind];
		return gameObjects[Random.Range(0, gameObjects.Count)];
	}

	private GameObject GetRandomGOFromFreeResourcePoints() {
		GameObject randomGameObject = allResourcePoints[Random.Range(0, allResourcePoints.Count)];

		var perpetualLoopProtection = 0;

		while(randomGameObject.transform.childCount != 0 && perpetualLoopProtection < 150) {
			randomGameObject = allResourcePoints[Random.Range(0, allResourcePoints.Count)];
			perpetualLoopProtection++;
		}

		return randomGameObject;
	}

	private void CreateMine(ResourceKind resourceKind) {
		GameObject mine            = mines.FirstOrDefault(a => a.GetComponent<Mine>().ResourceKind == resourceKind);
		GameObject instantiateMine = Instantiate(mine);
		switch(resourceKind) {
			case ResourceKind.FOOD :
				instantiateMine.GetComponent<TooltipText>().Text =
					instantiateMine.GetComponent<TooltipText>().HandWrittenText;
				instantiateMine.transform.position = platform.platforms[0].transform.position;
				CreateWorker(resourceKind, instantiateMine);
				break;
			case ResourceKind.TREE :
				instantiateMine.GetComponent<TooltipText>().Text =
					instantiateMine.GetComponent<TooltipText>().HandWrittenText;
				instantiateMine.transform.position = platform.platforms[1].transform.position;
				CreateWorker(resourceKind, instantiateMine);
				break;
			case ResourceKind.STONE :
				instantiateMine.GetComponent<TooltipText>().Text =
					instantiateMine.GetComponent<TooltipText>().HandWrittenText;
				instantiateMine.transform.position = platform.platforms[2].transform.position;
				CreateWorker(resourceKind, instantiateMine);
				break;
			case ResourceKind.GOLD :
				instantiateMine.GetComponent<TooltipText>().Text =
					instantiateMine.GetComponent<TooltipText>().HandWrittenText;
				instantiateMine.transform.position = platform.platforms[3].transform.position;
				CreateWorker(resourceKind, instantiateMine);
				break;
		}
	}

	private void CreateWorker(ResourceKind resourceKind, GameObject homeForWorker) {
		Worker  instantiate = Instantiate(worker);
		Vector3 homePos     = homeForWorker.transform.position;
		instantiate.transform.position = new Vector3(homePos.x - 1.5f, homePos.y, homePos.z);
		instantiate.ResourceKind       = resourceKind;
		instantiate.Home               = homeForWorker;
		instantiate.CurrentState       = StateWorker.FIND_RESOURCE;
		instantiate.SetTooltipText("-----");
	}
}
