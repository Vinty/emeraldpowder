﻿using System.Collections;
using System.Collections.Generic;
using QuickOutline.Scripts;
using UnityEngine;

public class Platform : MonoBehaviour {
    [SerializeField] public List<GameObject> platforms;

    private RaycastHit hitInfo;
    private GameObject currentPlatform;
    private bool       isMouseOnPlatform;
    private Coroutine  startCoroutineRadianceOn;

    private void Update() {
        RayCastMouseCheckForPlatformRadiance();
    }

    private void PlatformTagCompare() {
        if(!hitInfo.collider.gameObject.CompareTag("Platform")) {
            isMouseOnPlatform = false;
            return;
        }

        currentPlatform = hitInfo.collider.gameObject;

        if(isMouseOnPlatform == false) {
            SwitchRadiancePlatform();
            isMouseOnPlatform = true;
        }
    }

    private IEnumerator RadianceOf(Outline outline) {
        while(outline.OutlineWidth > 0) {
            outline.OutlineWidth -= 0.2f;
            yield return new WaitForSeconds(0.01f);
        }

        outline.isOutlineOn = false;
    }

    private IEnumerator RadianceOn(Outline outline) {
        while(outline.OutlineWidth < 5) {
            outline.OutlineWidth += 0.2f;
            yield return new WaitForSeconds(0.01f);
        }

        outline.isOutlineOn = true;
    }

    private void SwitchRadiancePlatform() {
        Outline outline = currentPlatform.GetComponent<Outline>();

        if(!outline.isOutlineOn) {
            startCoroutineRadianceOn = StartCoroutine(RadianceOn(outline));
            return;
        }

        startCoroutineRadianceOn = StartCoroutine(RadianceOf(outline));
    }

    private void RayCastMouseCheckForPlatformRadiance() {
        if(Camera.main == null) {
            return;
        }

        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if(Physics.Raycast(ray.origin, ray.direction, out hitInfo)) {
            PlatformTagCompare();

            if(isMouseOnPlatform) {
                return;
            }

            if(startCoroutineRadianceOn != null) {
                StopCoroutine(startCoroutineRadianceOn);
            }

            platforms.ForEach(x => x.GetComponent<Outline>().isOutlineOn = false);
            platforms.ForEach(x => StartCoroutine(RadianceOf(x.GetComponent<Outline>())));
        }
    }
}
