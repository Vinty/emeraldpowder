﻿using System;
using Env;
using UnityEngine;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour {
	[SerializeField] private Text lumberText;
	[SerializeField] private Text foodText;
	[SerializeField] private Text goldText;
	[SerializeField] private Text stoneText;

	public event Action<ResourceKind, string> LumberTextAction = delegate {};
	public event Action<ResourceKind, string> FoodTextAction   = delegate {};
	public event Action<ResourceKind, string> GoldTextAction   = delegate {};
	public event Action<ResourceKind, string> StoneTextAction  = delegate {};

	// public static bool IsGameStarted = true;

	public void AddResource(Resource resource) {
		Add(resource.ResKind, resource.ResourceCount);
	}

	private void Add(ResourceKind kind, int countResource) {
		switch(kind) {
			case ResourceKind.TREE :
				lumberText.text = AddQuantity(lumberText.text, countResource);
				LumberTextAction(ResourceKind.TREE, lumberText.text);
				break;
			case ResourceKind.FOOD :
				foodText.text = AddQuantity(foodText.text, countResource);
				FoodTextAction(ResourceKind.FOOD, foodText.text);
				break;
			case ResourceKind.GOLD :
				goldText.text = AddQuantity(goldText.text, countResource);
				GoldTextAction(ResourceKind.GOLD, goldText.text);
				break;
			case ResourceKind.STONE :
				stoneText.text = AddQuantity(stoneText.text, countResource);
				StoneTextAction(ResourceKind.STONE, stoneText.text);
				break;
		}
	}

	private string AddQuantity(string text, int countResource) {
		int oldText = int.Parse(text);
		return(countResource + oldText).ToString();
	}
}
