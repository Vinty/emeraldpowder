﻿using System.Collections.Generic;
using Env;
using UnityEngine;

namespace BuildingScripts {
    public class Mine : MonoBehaviour {
        [SerializeField] private ResourceKind resourceKind;

        public ResourceKind ResourceKind {get; set;}

        private FarmManager farmManager;

        private void Awake() {
            farmManager  = FindObjectOfType<FarmManager>();
            ResourceKind = resourceKind;
        }

        private Queue<Resource> extractedResourcesQueue = new Queue<Resource>();

        public Resource GetExtractedResourceFromQueue() {
            return extractedResourcesQueue.Count > 0 ? extractedResourcesQueue.Dequeue() : null;
        }

        public bool IsExtractedResourcesQueueNotEmpty() {
            return extractedResourcesQueue.Count > 0;
        }

        public void AddResourceToQueue(Resource resource) {
            extractedResourcesQueue.Enqueue(resource);
            farmManager.RegistrationMine(gameObject.GetComponent<Mine>());
        }
    }
}
