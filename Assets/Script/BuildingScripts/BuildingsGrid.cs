﻿using System;
using UnityEngine;

// ReSharper disable ReplaceWithSingleAssignment.True

namespace BuildingScripts {
    public class BuildingsGrid : MonoBehaviour {
        public int gridSizeX = 16;
        public int gridSizeY = 14;

        public  int leftX = 0;
        public  int downY = 0;
        private int rightX;
        private int upY;

        private Building[,] grid;
        private Building    flyingBuilding;
        private Camera      mainCamera;

        private void Awake() {
            rightX     = gridSizeX;
            upY        = gridSizeY;
            grid       = new Building[gridSizeX, gridSizeY];
            mainCamera = Camera.main;
        }

        public void StartPlacingBuilding(Building buildingPrefab) {
            if(flyingBuilding != null) {
                Destroy(flyingBuilding.gameObject);
            }

            flyingBuilding = Instantiate(buildingPrefab);
        }

        private void Update() {
            if(flyingBuilding == null) {
                return;
            }

            Plane groundPlane = new Plane(Vector3.up, Vector3.zero);
            Ray   ray         = mainCamera.ScreenPointToRay(Input.mousePosition);

            if(!groundPlane.Raycast(ray, out float position)) {
                return;
            }

            Vector3 worldPosition = ray.GetPoint(position);

            int x = Mathf.RoundToInt(worldPosition.x);
            int y = Mathf.RoundToInt(worldPosition.z);

            bool available = true;
            print("X = " + x + ";    y = " + y);

            if(x < leftX || x > rightX - flyingBuilding.size.x) {
                available = false;
            }

            if(y < downY || y > upY - flyingBuilding.size.y) {
                available = false;
            }

            if(available && IsPlaceTaken(x, y)) {
                available = false;
            }

            flyingBuilding.transform.position = new Vector3(x, 0, y);
            flyingBuilding.SetTransparent(available);

            if(available && Input.GetMouseButtonDown(0)) {
                PlaceFlyingBuildings(x, y);
            }
        }

        private bool IsPlaceTaken(int placeX, int placeY) {
            int leftXCorrect = Math.Sign(leftX) == -1 ? Mathf.Abs(leftX) : 0;
            int downYCorrect = Math.Sign(downY) == -1 ? Mathf.Abs(downY) : 0;

            for(int x = 0 ;
                x < flyingBuilding.size.x ;
                x++) {
                for(int y = 0 ;
                    y < flyingBuilding.size.y ;
                    y++) {
                    if(grid[placeX + x + leftXCorrect, placeY + y + downYCorrect] != null) {
                        return true;
                    }
                }
            }

            return false;
        }

        private void PlaceFlyingBuildings(int placeX, int placeY) {
            print("------");
            int leftXCorrect = Math.Sign(leftX) == -1 ? Mathf.Abs(leftX) : 0;
            int downYCorrect = Math.Sign(downY) == -1 ? Mathf.Abs(downY) : 0;
            for(int x = 0 ;
                x < flyingBuilding.size.x ;
                x++) {
                for(int y = 0 ;
                    y < flyingBuilding.size.y ;
                    y++) {
                    grid[placeX + x + leftXCorrect, placeY + y + downYCorrect] = flyingBuilding;
                }
            }

            flyingBuilding.SetNormal();
            flyingBuilding = null;
        }

        // private void OnDrawGizmos() {
        //     for(int x = 0 ;
        //         x < gridSizeX ;
        //         x++) {
        //         for(int y = 0 ;
        //             y < gridSizeY ;
        //             y++) {
        //             Gizmos.color = (x + y) % 2 == 0 ?
        //                 new Color(0.8f, 0.47f, 1f,    0.3f) :
        //                 new Color(1f,   0.99f, 0.25f, 0.3f);
        //             Gizmos.DrawCube(new Vector3(leftX, 0, downY) + new Vector3(x, 0, y), new Vector3(1, .1f, 1));
        //         }
        //     }
        // }
    }
}
