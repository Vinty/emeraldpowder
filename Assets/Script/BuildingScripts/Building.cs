﻿using UnityEngine;

namespace BuildingScripts {
    public class Building : MonoBehaviour {
        public Vector2Int size = Vector2Int.one;
        public Renderer   mainRenderer;

        public void SetTransparent(bool available) {
            mainRenderer.material.color = available ? Color.green : Color.red;
        }

        public void SetNormal() {
            mainRenderer.material.color = Color.white;
        }

        private void OnDrawGizmos() {
            for(int x = 0; x < size.x; x++) {
                for(int y = 0; y < size.y; y++) {
                    Gizmos.color = (x + y) % 2 == 0 ?
                        new Color(0.8f, 0.47f, 1f,    0.3f) :
                        new Color(1f,   0.99f, 0.25f, 0.3f);
                    Gizmos.DrawCube(transform.position + new Vector3(x, 0, y), new Vector3(1, .1f, 1));
                }
            }
        }
    }
}
