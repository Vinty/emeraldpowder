﻿namespace BuildingScripts {
    public class WareHouseLevel {
        private int warehouseLevel;
        private int goodsCount;
        private int porter;

        public WareHouseLevel(int warehouseLevel, int goodsCount, int porter) {
            this.warehouseLevel = warehouseLevel;
            this.goodsCount     = goodsCount;
            this.porter         = porter;
        }
    }
}
