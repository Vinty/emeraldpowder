﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using Env;
using UnityEngine;

namespace BuildingScripts {
	public class WareHouse : MonoBehaviour {
		private       MainMenu mainMenu;
		private const string   TOOLTIP_CONST_TEXT = "Warehouse \n ";

		private Dictionary<ResourceKind, string> dictionaryResources = new Dictionary<ResourceKind, string>();

		private void Awake() {
			GetComponent<TooltipText>().Text =  TOOLTIP_CONST_TEXT;
			mainMenu                         =  FindObjectOfType<MainMenu>();
			
			mainMenu.LumberTextAction        += SetTooltipText;
			mainMenu.FoodTextAction          += SetTooltipText;
			mainMenu.GoldTextAction          += SetTooltipText;
			mainMenu.StoneTextAction         += SetTooltipText;
		}

		private void SetTooltipText(ResourceKind resourceKind, string text) {
			AddResourceToDic(resourceKind, text);
			GetComponent<TooltipText>().Text = GetAllValueInString();
		}

		private string GetAllValueInString() {
			StringBuilder sb = new StringBuilder().Append(TOOLTIP_CONST_TEXT);
			dictionaryResources
			   .ToList()
			   .ForEach(pair => sb.Append($""                                   +
			                              $"{GetResourceKindString(pair.Key)} " +
			                              $"{pair.Value} \n"));
			return sb.ToString();
		}

		private void AddResourceToDic(ResourceKind resourceKind, string text) {
			if(!dictionaryResources.ContainsKey(resourceKind)) {
				dictionaryResources.Add(resourceKind, text);
				return;
			}

			dictionaryResources[resourceKind] = text;
		}

		private string GetResourceKindString(ResourceKind res) {
			switch(res) {
				case ResourceKind.STONE : return"Stone";
				case ResourceKind.GOLD :  return"Gold";
				case ResourceKind.FOOD :  return"Food";
				case ResourceKind.TREE :  return"Lumber";
			}

			return null;
		}
	}
}
