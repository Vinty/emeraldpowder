﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Threading;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Services;
using Google.Apis.Sheets.v4;
using Google.Apis.Sheets.v4.Data;
using Google.Apis.Util.Store;
using UnityEditor;
using UnityEngine;
using Color = UnityEngine.Color;
using Object = System.Object;

public class GetTablesFromGoogle : EditorWindow {
	[SerializeField] private string       spreadSheetKey   = "1cRNSJhfMnFCor-5pKvRnzgUzDXlPA6lHbbdTZNwVVOM";
	[SerializeField] private List<string> wantedSheetNames = new List<string>();
	[SerializeField] private string       outputDir        = "./Assets/Resources/JsonData/";

	static readonly string   CLIENT_ID     = "871414866606-7b9687cp1ibjokihbbfl6nrjr94j14o8.apps.googleusercontent.com";
	static readonly string   CLIENT_SECRET = "zF_J3qHpzX5e8i2V-ZEvOdGV";
	static          string[] Scopes        = { SheetsService.Scope.SpreadsheetsReadonly };

	private readonly string  appName = "Unity";
	private readonly string  urlRoot = "https://spreadsheets.google.com/feeds/spreadsheets/";
	private          Vector2 scrollPosition;
	private          float   progress        = 100;
	private          string  progressMessage = "";

	// [MenuItem("Utility/Прочитать данные из Goole-таблиц")]
	// private static void ShowWindow() {
	// 	GetTablesFromGoogle window = GetWindow(typeof(GetTablesFromGoogle)) as GetTablesFromGoogle;
	// }

	private void Init() {
		progress        = 100;
		progressMessage = "";

		ServicePointManager.ServerCertificateValidationCallback = CertificateValidation;
	}

	private void OnGUI() {
		Init();
		BeginGUIScrollPosition();
		GuiShow();
	}


	private void BeginGUIScrollPosition() {
		scrollPosition = EditorGUILayout.BeginScrollView(scrollPosition
		                                               , GUI.skin.scrollView
		                                               , GUILayout.Width(800)
		                                               , GUILayout.Height(600)
		);
	}


	private bool CertificateValidation(Object          sender
	                                 , X509Certificate certificate
	                                 , X509Chain       chain
	                                 , SslPolicyErrors sslPolicyErrors) {
		bool isValid = true;
		if(sslPolicyErrors == SslPolicyErrors.None) {
			return isValid;
		}

		foreach(X509ChainStatus t in chain.ChainStatus) {
			if(t.Status == X509ChainStatusFlags.RevocationStatusUnknown) {
				continue;
			}

			chain.ChainPolicy.RevocationFlag      = X509RevocationFlag.EntireChain;
			chain.ChainPolicy.RevocationMode      = X509RevocationMode.Online;
			chain.ChainPolicy.UrlRetrievalTimeout = new TimeSpan(0, 1, 0);
			chain.ChainPolicy.VerificationFlags   = X509VerificationFlags.AllFlags;
			bool chainIsValid = chain.Build((X509Certificate2) certificate);
			if(chainIsValid) {
				continue;
			}

			Debug.LogError("certificate chain is not valid");
			isValid = false;
		}

		return isValid;
	}

	public void GuiShow() {
		GUILayout.BeginVertical();
		{
			GUI.backgroundColor = new Color(0.74f, 0.69f, 0.25f, 0.15f);
			GUILayout.Label("Настройки", EditorStyles.boldLabel);

			EditorGUILayout.HelpBox(
				"Google-ключ - символы в командной строке Google-таблицы ПОСЛЕ символа /d/ и ДО /edit"
			  , MessageType.Info
			);
			spreadSheetKey = EditorGUILayout.TextField("Google-ключ",   spreadSheetKey);
			outputDir      = EditorGUILayout.TextField("Путь к файлам", outputDir);
			GUILayout.Label("");
			GUILayout.Label("Имена Листов", EditorStyles.boldLabel);
			EditorGUILayout.HelpBox(
				"Таблицы будут скачены целиком, если не указать только конкретные листы",
				MessageType.Info
			);

			int _removeId = -1;
			for(int i = 0 ;
				i < wantedSheetNames.Count ;
				i++) {
				GUILayout.BeginHorizontal();
				wantedSheetNames[i] = EditorGUILayout.TextField($"                    Лист {i}"
				                                              , wantedSheetNames[i]
				                                              , GUILayout.Width(300)
				);

				if(GUILayout.Button("X"
				                  , EditorStyles.miniButton
				                  , GUILayout.Width(20)
				)) { _removeId = i; }

				GUILayout.EndHorizontal();
			}

			if(_removeId >= 0) wantedSheetNames.RemoveAt(_removeId);
			GUILayout.Label(wantedSheetNames.Count <= 0
				                ? "Если не добавлять только нужные вам - скачаем все"
				                : $"                   Скачать только  эти листы. Колличество = {wantedSheetNames.Count}"
			);

			GUILayout.BeginArea(new Rect(10, 200, 40, 40));
			GUIStyle myButtonStyle = new GUIStyle(GUI.skin.button) { fontSize = 35 };
			GUI.backgroundColor         = new Color(0.42f, 0.65f, 0.71f, 0.45f);
			myButtonStyle.contentOffset = new Vector2(1, -7);
			if(GUILayout.Button("+"
			                  , myButtonStyle
			                  , GUILayout.Width(30)
			                  , GUILayout.Height(50)
			)) {
				wantedSheetNames.Add("");
			}

			GUILayout.EndArea();

			GUILayout.BeginArea(new Rect(340, 170, 255, 100));
			GUILayout.Label("");
			GUI.backgroundColor = new Color(0.42f, 0.65f, 0.71f, 0.45f);
			GUIStyle buttonStyle = new GUIStyle(GUI.skin.button);
			buttonStyle.fontSize = 18;
			if(GUILayout.Button("Скачать данные из Гугл-таблиц \nс последующей обработкой"
			                  , GUILayout.Width(250)
			                  , GUILayout.Height(60)
			)) {
				progress = 0;
				DownloadDataFromGoogle();
			}

			GUILayout.EndArea();

			GUI.backgroundColor = Color.white;
			if((progress < 100) && (progress > 0)) {
				if(EditorUtility.DisplayCancelableProgressBar("Обработка...", progressMessage,
				                                              progress / 100
				)) {
					progress = 100;
					EditorUtility.ClearProgressBar();
				}
			}
			else {
				EditorUtility.ClearProgressBar();
			}
		}

		try {
			GUILayout.EndVertical();
			EditorGUILayout.EndScrollView();
		}
		catch(Exception ex) {
			//Иногда, Unity пишет: "InvalidOperationException: Stack empty." 
		}
	}

	private void DownloadDataFromGoogle() {
		if(InputValidateSheetKey()) {
			return;
		}

		SheetsService service = Authenticate();
		ShowProgressBar();
		Spreadsheet  spreadSheetData = service.Spreadsheets.Get(spreadSheetKey).Execute();
		IList<Sheet> sheets          = spreadSheetData.Sheets;
		
		if(IsCheckSheetIsNull(sheets)) {
			return;
		}

		List<string> ranges = GetAllSheetNames(sheets);
		ReciveResponseFromGoogle(service, ranges);
	}

	private bool InputValidateSheetKey() {
		if(string.IsNullOrEmpty(spreadSheetKey)) {
			Debug.LogError("Ключ не может быть пустым!");
			return true;
		}

		return false;
	}

	private SheetsService Authenticate() {
		progressMessage = "Аунтификация...";
		var service = new SheetsService(new BaseClientService.Initializer() {
				HttpClientInitializer = GetCredential(), ApplicationName = appName,
			}
		);
		return service;
	}

	private UserCredential GetCredential() {
		MonoScript ms             = MonoScript.FromScriptableObject(this);
		string     scriptFilePath = AssetDatabase.GetAssetPath(ms);
		FileInfo   fi             = new FileInfo(scriptFilePath);
		string     scriptFolder   = fi.Directory.ToString();
		scriptFolder.Replace('\\', '/');
		Debug.Log("Сохраняем Credential в: " + scriptFolder);

		UserCredential credential    = null;
		ClientSecrets  clientSecrets = new ClientSecrets();
		clientSecrets.ClientId     = CLIENT_ID;
		clientSecrets.ClientSecret = CLIENT_SECRET;
		try {
			credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
				clientSecrets,
				Scopes,
				"user",
				CancellationToken.None,
				new FileDataStore(scriptFolder, true)
			).Result;
		}
		catch(Exception e) {
			Debug.LogError(e.ToString());
		}

		return credential;
	}

	private void ShowProgressBar() {
		progress = 5;
		EditorUtility.DisplayCancelableProgressBar("Скачиваем данные из Гугл-Документа", progressMessage, progress / 100
		);
		progressMessage = "Получение списка всех листов...";
		EditorUtility.DisplayCancelableProgressBar("Подождите...", progressMessage, progress / 100);
	}

	private bool IsCheckSheetIsNull(IList<Sheet> sheets) {
		if((sheets == null) || (sheets.Count <= 0)) {
			Debug.LogError("Данные не найдены!");
			progress = 100;
			EditorUtility.ClearProgressBar();
			return true;
		}

		return false;
	}

	private List<string> GetAllSheetNames(IList<Sheet> sheets) {
		return (from sheet in sheets 
			where (wantedSheetNames.Count <= 0) || (wantedSheetNames.Contains(sheet.Properties.Title)) 
			select sheet.Properties.Title).ToList();
	}

	private void ReciveResponseFromGoogle(SheetsService service, List<string> ranges) {
		progress = 50;
		SpreadsheetsResource.ValuesResource.BatchGetRequest request =
			service.Spreadsheets.Values.BatchGet(spreadSheetKey);
		request.Ranges = ranges;
		BatchGetValuesResponse response = request.Execute();

		foreach(ValueRange vRange in response.ValueRanges) {
			string sheetName = vRange.Range.Split('!')[0];
			progressMessage = $"Считывается {sheetName}";
			EditorUtility.DisplayCancelableProgressBar("Подождите", progressMessage, progress / 100);

			// CreateJsonFile(Sheetname, outputDir, valueRange);

			progress = wantedSheetNames.Count <= 0
				? progress += 50 / response.ValueRanges.Count
				: progress += 50 / wantedSheetNames.Count;
		}
	}
}
