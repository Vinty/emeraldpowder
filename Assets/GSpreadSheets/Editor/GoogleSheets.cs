﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Services;
using Google.Apis.Sheets.v4;
using Google.Apis.Sheets.v4.Data;
using Google.Apis.Util.Store;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;
using Color = UnityEngine.Color;
using Object = System.Object;

namespace GSpreadSheets {

	class GoogleSheets : EditorWindow {
		[SerializeField] private string spreadsheetId         = "1cRNSJhfMnFCor-5pKvRnzgUzDXlPA6lHbbdTZNwVVOM";
		[SerializeField] private string googleCredentialsPath = "./Assets/credentials.json";

		private       VisualElement visualElement;
		private const string        UXML_FILE_PATH = "Assets/GSpreadSheets/Editor/GoogleSheets.uxml";

		static string[] Scopes          = { SheetsService.Scope.Spreadsheets };
		static string   ApplicationName = "Google Sheets API by Vinty";

		private SheetsService service;

		private float              progress        = 100;
		private string             progressMessage = "";
		private Vector2            scrollPosition;
		private List<string>       currentListSheets;
		private List<string>       lastListSheets;
		private PopupField<string> popupField;

		[MenuItem("Utility/Читать из Google-таблиц")]
		private static void ShowWindow() {
			GoogleSheets window = GetWindow<GoogleSheets>();
			window.titleContent = new GUIContent("GoogleSheets");
		}

		private void OnEnable() {
			GuiUxmlShow();
			ButtonControls();
		}

		private void OnGUI() {
			Init();
			BeginGUIScrollPosition();
			GUI.EndGroup();
		}

		private void GuiUxmlShow() {
			VisualTreeAsset uiTemplate = AssetDatabase.LoadAssetAtPath<VisualTreeAsset>(UXML_FILE_PATH);
			var             ui         = uiTemplate.Instantiate();
			rootVisualElement.Add(ui);
		}

		private void ButtonControls() {
			Button refreshListButton = rootVisualElement.Q<Button>("ShowSheets");
			refreshListButton.clickable.clicked += () => {
				                                       currentListSheets = GetListSheets();
				                                       ShowGUIPopupFromGoogleSheet();
			                                       };
			Button getContentListButton = rootVisualElement.Q<Button>("GetContentListButton");
			getContentListButton.clickable.clicked += () => {
				                                  VisualElement element = rootVisualElement.Q<VisualElement>("ListPopupField");
				                                  IEnumerable<VisualElement> visualElements = element.Children();
				                                  IEnumerable<PopupField<string>> popupFields =
					                              visualElements.OfType<PopupField<string>>();
				                                  GetListByNameGoogleSheet(popupFields.ToList()[0].value);};
		}

		private void ShowGUIPopupFromGoogleSheet() {
			if(popupField != null && currentListSheets.SequenceEqual(lastListSheets)) {
				return;
			}

			if(popupField != null) {
				rootVisualElement
				   .Q<VisualElement>("ListPopupField").Remove(popupField);
			}

			lastListSheets = GetListSheets();
			popupField     = new PopupField<string>("Листы", currentListSheets, 0);

			rootVisualElement
			   .Q<VisualElement>("ListPopupField")
			   .Add(popupField);
		}

		private void Init() {
			progress        = 100;
			progressMessage = "";
			UserCredential credential;

			using(var stream = new FileStream(googleCredentialsPath, FileMode.Open, FileAccess.ReadWrite)) {
				string credPath = "Assets/token.json";
				credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
					GoogleClientSecrets.Load(stream).Secrets
				  , Scopes
				  , "user"
				  , CancellationToken.None
				  , new FileDataStore(credPath, true)
				).Result;
			}

			service = new SheetsService(new BaseClientService.Initializer() {
					HttpClientInitializer = credential, ApplicationName = ApplicationName,
				}
			);
		}

		private void BeginGUIScrollPosition() {
			scrollPosition = EditorGUILayout.BeginScrollView(scrollPosition
			                                               , GUI.skin.scrollView
			                                               , GUILayout.Width(600)
			                                               , GUILayout.Height(600)
			);
		}

		// CreateSheet(service, "Vinty-List-1", spreadsheetId);

		// UpdateSheet(service
		//           , spreadsheetId
		//           , "Vinty-List-1"
		//           , new List<IList<object>>() { new List<object>() { "Vinty-1", "Vinty-2", "Vinty-3" } }
		// );

		private void GetListByNameGoogleSheet(string nameGoogleSheet) {
			RemoveOldTableVisualElement();

			string range = $"{nameGoogleSheet}!A1:H";

			SpreadsheetsResource.ValuesResource.GetRequest request =
				service.Spreadsheets.Values.Get(spreadsheetId, range);
			ValueRange           response = request.Execute();
			IList<IList<Object>> values   = response.Values;
			if(values != null && values.Count > 0) {
				// TODO вставить логику получения листа
				values.ToList().ForEach(
					x => x.ToList()
					      .ForEach(content => {
						               AddTableVisualElement(content.ToString());
					               }
					       )
				);
			}
		}

		private void AddTableVisualElement(string textValue) {
			TextField textField = new TextField { value = textValue };

			rootVisualElement
			   .Q<VisualElement>("ContentListVisualElement")
			   .Add(textField);
		}

		private void RemoveOldTableVisualElement() {
			VisualElement              element        = rootVisualElement.Q<VisualElement>("ContentListVisualElement");
			IEnumerable<VisualElement> visualElements = element.Children();
			visualElements
			   .ToList()
			   .ForEach(x => rootVisualElement.Q<VisualElement>("ContentListVisualElement").Remove(x));
		}

		private List<string> GetListSheets() {
			Spreadsheet  spreadSheetData = service.Spreadsheets.Get(spreadsheetId).Execute();
			IList<Sheet> sheets          = spreadSheetData.Sheets;
			return sheets.Select(x => x.Properties.Title).ToList();
		}

		private void UpdateSheet(SheetsService        service, string spreadsheetId, string inList,
		                         IList<IList<object>> body) {
			String spreadsheetID = spreadsheetId;
			String range         = $"{inList}!A2";

			ValueRange valueRange = new ValueRange();
			valueRange.MajorDimension = "COLUMNS"; //"ROWS";//COLUMNS
			valueRange.Values         = body;

			SpreadsheetsResource.ValuesResource.UpdateRequest update =
				service.Spreadsheets.Values.Update(valueRange, spreadsheetID, range);
			update.ValueInputOption = SpreadsheetsResource.ValuesResource.UpdateRequest.ValueInputOptionEnum.RAW;
			UpdateValuesResponse updateValuesResponse = update.Execute();
			// print(JsonConvert.SerializeObject(updateValuesResponse));
		}

		private void CreateSheet(SheetsService service, string newListName, string keySheet) {
			AddSheetRequest addSheetRequest = new AddSheetRequest();
			addSheetRequest.Properties       = new SheetProperties();
			addSheetRequest.Properties.Title = newListName;
			BatchUpdateSpreadsheetRequest batchUpdateSpreadsheetRequest = new BatchUpdateSpreadsheetRequest();
			batchUpdateSpreadsheetRequest.Requests = new List<Request>();
			batchUpdateSpreadsheetRequest.Requests.Add(new Request { AddSheet = addSheetRequest });

			var batchUpdateRequest =
				service.Spreadsheets.BatchUpdate(batchUpdateSpreadsheetRequest, keySheet);

			batchUpdateRequest.Execute();
		}
	}
}
